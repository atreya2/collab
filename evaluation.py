from ev import *
from whoosh import fields, index, scoring, qparser
from whoosh.index import *
from whoosh.fields import *
from whoosh.qparser import *
from whoosh.scoring import *
from heapq import nlargest
from operator import itemgetter
import random


def calc_auth_scores(score_d, filename, algo, auth_list):
	#create dictionary for author_name
	auth_dict = {}
	with open("results/author.txt") as f:
		for l in f:
			l = l.strip().split("\t")
			auth_dict[l[0]] = l[1]

	auth_ws = {}
	auth_w = {}
	output_list = []
	
	with open("results/"+filename+".txt") as f:
		for line in f:
			l = line.strip().split("\t")
			paper = l[0]
			author = l[1]
			wght = float(l[2])
			if paper in score_d:
				if author not in auth_ws:
					auth_ws[author] = wght*score_d[paper]
					auth_w[author] = wght
				else:
					auth_ws[author] += wght*score_d[paper]
					auth_w[author] += wght
	for auth in auth_ws:
		auth_ws[auth] = auth_ws[auth]/auth_w[auth]

	for auth, score in nlargest(20, auth_ws.iteritems(), key=itemgetter(1)):
		output_list.append(auth_dict[auth])
	tup = evaluate(auth_list, output_list, 20)

	return tup


def bm25(query, auth_list):
	# '***** BM 25 **** '
	
	schema = fields.Schema(paper_id=fields.NUMERIC(stored=True, sortable=True), paper_txt=fields.TEXT(stored=True))
	ix = open_dir("index/title_index")
	score_d={}
	parser = qparser.QueryParser("paper_txt", schema=schema, group=qparser.OrGroup)
	with ix.searcher() as searcher:
		# query = QueryParser("paper_txt", ix.schema).parse(u'kernel density function is a funny thing hahah')
		query = parser.parse(unicode(query))
		results = searcher.search(query, limit = 728455)
		
		for i, hit in enumerate(results):
			score_d[hit['paper_id']] = results.score(i)

	res = calc_auth_scores(score_d, "sim_PA", "bm25", auth_list)
	#calc_auth_scores(score_d, "sim_PAPA", "bm25", auth_list)
	#calc_auth_scores(score_d, "sim_PVPA", "bm25", auth_list)
	return res

def tfidf(query, auth_list):
	#'***** TF IDF **** '
	schema = fields.Schema(paper_id=fields.NUMERIC(stored=True, sortable=True), paper_txt=fields.TEXT(stored=True))	
	ix = open_dir("index/title_index")
	score_tf_d={}
	parser = qparser.QueryParser("paper_txt", schema=schema, group=qparser.OrGroup)
	with ix.searcher(weighting=scoring.TF_IDF()) as searcher:
		# query = QueryParser("paper_txt", ix.schema).parse(u'kernel density function is a funny thing hahah')
		query = parser.parse(unicode(query))
		results = searcher.search(query, limit = 728455)
		for i, hit in enumerate(results):
			score_tf_d[hit['paper_id']] = results.score(i)

	#calc_auth_scores(score_tf_d, "sim_PA", "tfidf", auth_list)
	#calc_auth_scores(score_tf_d, "sim_PAPA", "tfidf", auth_list)
	res =  calc_auth_scores(score_tf_d, "sim_PVPA", "tfidf", auth_list)
	return res


def main():
	pid_title = {}
	with open("results/paper_title.txt") as f:
		for line in f:
			l=line.strip().split("\t")
			pid_title[l[1]] = l[0]

	auth_dict = {}
	with open("results/author.txt") as f:
		for l in f:
			l = l.strip().split("\t")
			auth_dict[l[0]] = l[1]		

	pid_auth = {}
	with open("results/paper_author.txt") as f:
		for line in f:
			l=line.strip().split("\t")
			paper = l[0]
			auth = l[1]
			if paper in pid_auth:
				l = pid_auth[paper]
				l.append(auth_dict[auth])
				pid_auth[paper] = l
			else:
				auth_l=[]
				auth_l.append(auth_dict[auth])
				pid_auth[paper] = auth_l

	
	r_list = random.sample(pid_title.keys(), 100)

	precision = []
	recall = []
	f = []
	for p in r_list:

		result = tfidf(p, pid_auth[pid_title[p]])
		precision.append(result[0])
		recall.append(result[1])
		f.append(result[2])
	


	avg_pre = sum(precision) / float(len(precision))
	avg_recall = sum(recall) / float(len(recall))
	avg_f = sum(f) / float(len(f))
	print(avg_pre, avg_recall, avg_f)	







if __name__ == '__main__':
	main()