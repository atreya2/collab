import numpy
from sklearn.metrics import pairwise as p
from sklearn import preprocessing as pre

def normalize_array(a):
	row_sums = a.sum(axis=1)
	new_m = a / row_sums[:, numpy.newaxis]
	return new_m

pa = []
pv = []
pt = []
pe = []
ve = []

ae1 = []
pe1 = []

out = open('data/pvpa.txt', 'w')
out1 = open('data/papa.txt', 'w')

author_d = {}
d_author = {}
a_count = 0
with open("data/paper_author.txt") as f:
	for line in f:
		l = line.strip().split("\t")
		if l[1] not in author_d:
			author_d[l[1]] = a_count
			d_author[a_count] = l[1]
			a_count += 1

paper_d = {}
d_paper = {}
p_count = 0
with open("data/paper_title.txt") as f:
	for line in f:
		l = line.strip().split("\t")
		if l[0] not in paper_d:
			paper_d[l[0]] = p_count
			d_paper[p_count] = l[0]
			p_count += 1

pa_count = 0
with open("data/paper_author.txt") as f:
	for line in f:
		l = line.strip().split("\t")
		pe1.append((paper_d[l[0]], pa_count))
		ae1.append((author_d[l[1]], pa_count))
		pa_count+=1

term_d = {}
term_count = 0
with open("data/paper_title.txt") as f:
	for line in f:
		l = line.strip().split("\t")
		terms=l[1].split(" ")
		for term in terms:
			if term not in term_d:
				term_d[term] = term_count
				term_count += 1

venue_d = {}
venue_count = 0
with open("data/paper_venue.txt") as f:
	for line in f:
		l = line.strip().split("\t")
		if l[1] not in venue_d:
			venue_d[l[1]] = venue_count
			venue_count += 1

e_count=0
with open("data/paper_venue.txt") as f:
	for line in f:
		l = line.strip().split("\t")
		pe.append((paper_d[l[0]], e_count))
		ve.append((venue_d[l[1]], e_count))
		e_count+=1


print(len(paper_d), len(venue_d))
with open("data/paper_author.txt") as f:
	for line in f:
		l = line.strip().split("\t")
		pa.append((paper_d.get(l[0]), author_d.get(l[1])))

with open("data/paper_title.txt") as f:
	for line in f:
		l = line.strip().split("\t")
		terms = l[1].split(" ")
		for term in terms:
			pt.append((paper_d.get(l[0]), term_d.get(term)))

with open("data/paper_venue.txt") as f:
	for line in f:
		l = line.strip().split("\t")
		pv.append((paper_d.get(l[0]), venue_d.get(l[1])))
               


paper_author = numpy.zeros((len(paper_d), len(author_d)))
paper_venue = numpy.zeros((len(paper_d), len(venue_d)))
paper_term = numpy.zeros((len(paper_d), len(term_d)))
paper_e = numpy.zeros((len(paper_d), len(paper_d)))
venue_e = numpy.zeros((len(venue_d), len(paper_d)))

for i in range(0, len(pa)):
	paper_author[pa[i][0]][pa[i][1]] = 1.0

for i in range(0, len(pt)):
	paper_term[pt[i][0]][pt[i][1]] = 1.0

for i in range(0, len(pv)):
	paper_venue[pv[i][0]][pv[i][1]] = 1.0

#PVPA metapath
for i in range(0, len(pe)):
	paper_e[pe[i][0]][pe[i][1]] = 1.0

for i in range(0, len(ve)):
	venue_e[ve[i][0]][ve[i][1]] = 1.0

author_paper = numpy.transpose(paper_author)
author_paper = normalize_array(author_paper)
paper_venue = normalize_array(paper_venue)
venue_e = normalize_array(venue_e)
paper_e = normalize_array(paper_e)

PE = numpy.dot(paper_venue, venue_e) 
AE = numpy.dot(author_paper, paper_e)

cos_sim = p.cosine_similarity(PE,AE)

for (i,j), x in numpy.ndenumerate(cos_sim):
	if x>0.01:
		out.write(str(d_paper[i]) + "\t" + str(d_author[j]) + "\t" + str(x) + "\n")

#PAPA metapath
paper_e1 = numpy.zeros((len(paper_d), pa_count))
author_e1 = numpy.zeros((len(author_d), pa_count))

for i in range(0, len(pe1)):
	paper_e1[pe1[i][0]][pe1[i][1]] = 1.0

for i in range(0, len(ae1)):
	author_e1[ae1[i][0]][ae1[i][1]] = 1.0


paper_author = normalize_array(paper_author)
author_e1 = normalize_array(author_e1)
paper_e1 = normalize_array(paper_e1)

PE1 = numpy.dot(paper_author, author_e1) 
AE1 = numpy.dot(author_paper, paper_e1)

cos_sim1 = p.cosine_similarity(PE1,AE1)

for (i,j), x in numpy.ndenumerate(cos_sim1):
	if x>0.01:
		out1.write(str(d_paper[i]) + "\t" + str(d_author[j]) + "\t" + str(x) + "\n")