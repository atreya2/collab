import random

def main():
	data_f = open('citation-network2.txt')

	authors=[]
	abstract=""
	venue=""
	title=""
	paper=0
	auth_dict={}
	d_count=0
	count=1
	authors_f = open('results/author.txt', 'w')
	p_a = open('results/paper_author.txt', 'w')
	p_a_s = open('results/sim_PA.txt', 'w')
	p_v = open('results/paper_venue.txt', 'w')
	p_t = open('results/paper_title.txt', 'w')
	p_ab = open('results/paper_abstract.txt', 'w')
	t_p = open('results/title_paper.txt', 'w')

	test = open('results/test_title.txt', 'w')
	test_title = open('results/test_title_name.txt', 'w')

	ran_list = random.sample(range(5000), 100)
	for line in data_f:
		line = line.rstrip()
		if line.startswith('#*'):
			title = line[2:]
			
		if line.startswith('#@'):
			line = line[2:]
			
			if ',' in line:
				split_auth = line.split(",")
				for author in split_auth:
					authors.append(author)
			else:
				authors.append(line)

		if line.startswith('#index'):
			paper = line[6:]

		if line.startswith('#!'):
			abstract = line[2:]

		if line.startswith('#c'):
			venue = line[2:]	

		if line=="":
			if authors!="" and abstract!="" and title!="" and paper!=0 and len(authors)!=0 and venue!="":
				# if d_count in ran_list:
				# 	for auth in authors:
				# 		test.write(str(paper) + "\t" + auth + '\n')
				# 	test_title.write(str(paper) + "\t" + title + '\n')	

				# else:
				for auth in authors:
					if auth not in auth_dict:
						auth_dict[auth]=count
						count+=1
						authors_f.write(str(auth_dict[auth]) + "\t" + auth + '\n')
					p_a.write(str(paper) + "\t" + str(auth_dict[auth]) + '\n')
					p_a_s.write(str(paper) + "\t" + str(auth_dict[auth]) + "\t" + str(1.0) + '\n')

				
				p_t.write(str(paper) + "\t" + title + '\n')
				t_p.write(title + "\t" + str(paper) + '\n')
				p_ab.write(str(paper) + "\t" + abstract + '\n')
				p_v.write(str(paper) + "\t" + venue + '\n')
				d_count+=1
			authors=[]
			abstract=""
			venue=""
			title=""
			paper=0
			if d_count==5000:
				break
	authors_f.close()
	p_a.close()
	p_v.close()
	p_t.close()
	p_ab.close()














if __name__ =='__main__':main() 
