
def evaluate(author_list, output, k):

	common = []
	i=0
	while i<len(output) and i<k:
		common.append(output[i])
		i+=1

	intersect = list(set(author_list) & set(common)) 

	count=len(intersect)

	if k!=0:
		prec = float(count)/float(k)


	recall = float(count)/float(len(author_list))

	if prec==0 and recall==0:
		f=0
	else:
		f = 2*prec*recall/(prec+recall)

	return ((prec, recall, f))
