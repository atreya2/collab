#clone the project
	$ git clone https://gitlab-beta.engr.illinois.edu/atreya2/collab.git
	
Once, you have downloaded the repository, ensure that you have downloaded the necessary packages (requirements) - 
whoosh
Sklearn
	
	$ pip install whoosh
	
Input : query (title/abstract). The query should be entered in a text file and our system takes the name of this file as a parameter. But the file should be placed in the same folder i.e. /collab. By default, the query has been placed in the file input.txt.


$ cd collab


Create text file in this folder with any name and enter the query in this file.


Author search: For author search, use the following command - 
 
$ python author_search.py input.txt bm25 pa 10


There are four arguments in this command - 
input.txt - name of input file that contains the query
bm25 - ranking function, can be either tfidf or bm25
pa - meta path, can be one of the three paths - pa, papa, pvpa
10 - number of results


Paper search: For paper search, use the following command - 
 
$ python paper_search.py input.txt bm25 10


There are three arguments in this command - 
input.txt - name of input file that contains the query
bm25 - ranking function, can be either tfidf or bm25
10 - number of results


Output: For demo purpose, the output is displayed on the command line and would be a ranked list of authors or papers.
	
	Optional: The following steps are involved if you want to use your own data set - 
Create index
Generate PA, PAPA, PVPA files using HeteSim
Paper search/author search using any query
	
	First step is to create index from the data - 
	$ cd collab
If you want to use your own data set, all the preprocessed files (described above) should be present in the /data directory.  
	
	Then, to create index, use the following command (empty the index folder before this) - 
	
$ python create_index.py


The index is created using the whoosh library and is present in /index.


Now, in order to search using any of the meta paths (i.e. PA, PVPA, PAPA), you need to execute hetesim.py that generates these files in /data.


After this step, you can search for author/paper using the commands mentioned above.