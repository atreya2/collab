from whoosh import fields, index, scoring, qparser
from whoosh.index import *
from whoosh.fields import *
from whoosh.qparser import *
from whoosh.scoring import *
from heapq import nlargest
from operator import itemgetter
import sys


def calc_auth_scores(score_d, filename, n):
	#create dictionary for author_name
	auth_dict = {}
	with open("data/author.txt") as f:
		for l in f:
			l = l.strip().split("\t")
			auth_dict[l[0]] = l[1]

	auth_ws = {}
	auth_w = {}
	
	with open("data/"+filename+".txt") as f:
		for line in f:
			l = line.strip().split("\t")
			paper = l[0]
			author = l[1]
			wght = float(l[2])
			if paper in score_d:
				if author not in auth_ws:
					auth_ws[author] = wght*score_d[paper]
					auth_w[author] = wght
				else:
					auth_ws[author] += wght*score_d[paper]
					auth_w[author] += wght
	for auth in auth_ws:
		auth_ws[auth] = auth_ws[auth]/auth_w[auth]
	for auth, score in nlargest(n, auth_ws.iteritems(), key=itemgetter(1)):
		print(str(auth_dict[auth]) + "\t" + str(score))


def bm25(query, agg_func, n):
	# '***** BM 25 **** '
	
	schema = fields.Schema(paper_id=fields.NUMERIC(stored=True, sortable=True), paper_txt=fields.TEXT(stored=True))
	ix = open_dir("index/title_index")
	score_d={}
	parser = qparser.QueryParser("paper_txt", schema=schema, group=qparser.OrGroup)
	with ix.searcher() as searcher:
		query = parser.parse(unicode(query))
		results = searcher.search(query, limit = 728455)
		
		for i, hit in enumerate(results):
			score_d[hit['paper_id']] = results.score(i)
	calc_auth_scores(score_d, agg_func, n)


def tfidf(query, agg_func, n):
	#'***** TF IDF **** '
	schema = fields.Schema(paper_id=fields.NUMERIC(stored=True, sortable=True), paper_txt=fields.TEXT(stored=True))
	ix = open_dir("index/title_index")
	score_tf_d={}
	parser = qparser.QueryParser("paper_txt", schema=schema, group=qparser.OrGroup)
	with ix.searcher(weighting=scoring.TF_IDF()) as searcher:
		query = parser.parse(unicode(query))
		results = searcher.search(query, limit = 728455)
		for i, hit in enumerate(results):
			score_tf_d[hit['paper_id']] = results.score(i)

	calc_auth_scores(score_tf_d, agg_func, n)		


def main():

	input_f = open(sys.argv[1])
	query=input_f.read()
	algo = sys.argv[2]
	agg_function = sys.argv[3]
	no_of_results = int(sys.argv[4])

	if algo.lower()=="bm25":
		bm25(query, agg_function, no_of_results)
	elif algo.lower()=="tfidf":
		tfidf(query, agg_function, no_of_results)
	else:
		print("Invalid arguments. Try again.")	


	

if __name__ == '__main__':
	main()