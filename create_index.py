import os
from whoosh import fields, index

def main():
	schema = fields.Schema(paper_id=fields.NUMERIC(stored=True, sortable=True), paper_txt=fields.TEXT(stored=True))
	os.system('mkdir index/title_index')
	ix = index.create_in("index/title_index", schema)
	w = ix.writer()

	f_paper = open('data/paper_title.txt')
	ctr = 0
	for line in f_paper:
		line = line.strip()
		pid = line.split('\t')[0]
		ptxt= unicode(line.split('\t')[1], errors='replace')
		# print pid, ptxt
		w.add_document(paper_id=pid, paper_txt=ptxt)
		ctr+=1
	f_paper.close()
	w.commit()


if __name__ == '__main__':
	main()