from whoosh import fields, index, scoring, qparser
from whoosh.index import *
from whoosh.fields import *
from whoosh.qparser import *
from whoosh.scoring import *
from heapq import nlargest
from operator import itemgetter


def bm25(query, pid_title, n):
	# '***** BM 25 **** '
	schema = fields.Schema(paper_id=fields.NUMERIC(stored=True, sortable=True), paper_txt=fields.TEXT(stored=True))
	ix = open_dir("index/title_index")
	score_d={}
	parser = qparser.QueryParser("paper_txt", schema=schema, group=qparser.OrGroup)
	with ix.searcher() as searcher:
		query = parser.parse(unicode(query))
		results = searcher.search(query, limit = 728455)
		
		for i, hit in enumerate(results):
			score_d[hit['paper_id']] = results.score(i)

		for pid, score in nlargest(n, score_d.iteritems(), key=itemgetter(1)):
			print(str(pid_title[pid]) + "\t" + str(score))



def tfidf(query, pid_title, n):
	#'***** TF IDF **** '
	schema = fields.Schema(paper_id=fields.NUMERIC(stored=True, sortable=True), paper_txt=fields.TEXT(stored=True))
	ix = open_dir("index/title_index")
	score_tf_d={}
	parser = qparser.QueryParser("paper_txt", schema=schema, group=qparser.OrGroup)
	with ix.searcher(weighting=scoring.TF_IDF()) as searcher:
		query = parser.parse(unicode(query))
		results = searcher.search(query, limit = 728455)
		for i, hit in enumerate(results):
			score_tf_d[hit['paper_id']] = results.score(i)

		for pid, score in nlargest(n, score_tf_d.iteritems(), key=itemgetter(1)):
			print(str(pid_title[pid]) + "\t" + str(score))


def main():

	input_f = open("input.txt")
	query=input_f.read()

	pid_title = {}
	with open("data/paper_title.txt") as f:
		for line in f:
			l=line.strip().split("\t")
			pid_title[l[0]] = l[1]


	input_f = open(sys.argv[1])
	query=input_f.read()
	algo = sys.argv[2].lower()
	no_of_results = int(sys.argv[3])
	if algo=="bm25":
		bm25(query, pid_title, no_of_results)
	elif algo=="tfidf":
		tfidf(query, pid_title, no_of_results)
	else:
		print("Invalid arguments. Try again.")


	
if __name__ == '__main__':
	main()